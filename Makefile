CARGO_PATH = /Users/pawelkozik/.cargo/bin/cargo

build-ajs:
	cd ./plugin-ajs && npm ci && npm run asbuild:optimized

start-ajs:
	func-e run -c ./envoy-ajs.yaml

build-rust:
	cd plugin-rust && PATH="/opt/homebrew/opt/llvm/bin:$PATH" CC=/opt/homebrew/opt/llvm/bin/clang AR=/opt/homebrew/opt/llvm/bin/llvm-ar ${CARGO_PATH} build --target wasm32-unknown-unknown --release

start-rust:
	func-e run -c ./envoy-rust.yaml

build-go:
	cd ./plugin-go && tinygo build -o optimized.wasm -scheduler=none -target=wasi ./main.go

start-go:
	func-e run -c ./envoy-go.yaml

test:
	curl -I http://127.0.0.1:8080

.PHONY: build-ajs start-ajs build-rust start-rust build-go start-go test
