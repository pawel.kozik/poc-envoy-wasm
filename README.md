# PoC Envoy WASM plugin

## Pre-requisites

- [func-e](https://formulae.brew.sh/formula/func-e)
- Nodejs

## Installation

```bash
$ scripts/build.sh
```

## Run

Run Envoy

```bash
$ ./scripts/start.sh
```

Test plugin

```bash
$ ./scripts/test.sh
```
