extern crate rsa;
extern crate pkcs8;
extern crate sha2;

use log::info;
use proxy_wasm as wasm;

use signature::Signer;

use rsa::pkcs1v15::SigningKey;
use rsa::RsaPrivateKey;
use pkcs8::DecodePrivateKey;

use sha2::Sha256;

use core::num::NonZeroU32;
use getrandom::{register_custom_getrandom, Error};

// Some application-specific error code
const MY_CUSTOM_ERROR_CODE: u32 = Error::CUSTOM_START + 42;
pub fn always_fail(_buf: &mut [u8]) -> Result<(), Error> {
    let code = NonZeroU32::new(MY_CUSTOM_ERROR_CODE).unwrap();
    Err(Error::from(code))
}

register_custom_getrandom!(always_fail);

#[no_mangle]
pub fn _start() {
    proxy_wasm::set_log_level(wasm::types::LogLevel::Trace);
    proxy_wasm::set_http_context(
        |context_id, _root_context_id| -> Box<dyn wasm::traits::HttpContext> {
            Box::new(HelloWorld { context_id })
        },
    )
}

struct HelloWorld {
    context_id: u32,
}

impl wasm::traits::Context for HelloWorld {}

impl wasm::traits::HttpContext for HelloWorld {
    fn on_http_request_headers(&mut self, num_headers: usize) -> wasm::types::Action {
        let pem = "-----BEGIN PRIVATE KEY-----
MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCoHRSZu/TBrWNn
ndR8uG1O4yQQbB4N1++eGVxmoj7/4r6EKnYpImH6Kd02vboW1NI3o0EuBMluE6fr
+/mY2DraapHRVTqiT28mkWD2JBcCQJ9JC4Jo5BfdZmH1zGv24huWNTdF46uOMCtB
Ie2l55CTJTWf6/VZt9nkPjAZ0Vl4EV4762iECQSO5GR62Lkcgfssah6nF2lccUlY
4bIsa0pgQo63GSuFTVBPxrQnu6T4N4ILXgntLv8FyA/a/qhdxWwrJraD7M5nT0UE
8pl3YO4dwZlmPVtKtEt2qEFoRDEJSZDIBqGOnkhxnS9pWr+zCRpWNvv6DvaQxRAv
FzRzNA3FAgMBAAECggEAC3EHO5Cjj3bp2Ah8M2IollY2gyOd8VqWmqT74+77k1YZ
mfcl2ZYseDvXLBLs56w5A8O0UO8kd73Auk/eOefQLENVL0Zkr4fxIafX+IiWREyk
9UdiQG9IVy97pUtHYc3GYN+4UPpgwlRG7Vo4yVzMOyeCJyMxAe0rAty0pgfYgKeu
2qInTtylfpa7WwdfVnr9XmKIjP+uiCkFmCAwAHQSLLhzmozZI65916ZooRd9eQbI
hms/X+e8wuWnVQvylc3o78m7kJkR6OI88XhqG0isTVGMmtl7zPi/RojHKS6/X7Ui
kx6LYhAcWC52y6aylJ6DtjLlGKeuDezfMjKpk7qAwQKBgQDpM6kNjiVL70UKQNQ6
Wc30JVuDaNCwSI+mERdS08kpjRWS9z3+QP8OJkO6V7p03YG8n11V+cf4gL8GJ/jd
WfEEJar9/ERNLDZQF9TyAezPfCgi8NcvpjLEQau1oC57JYfbjONQoOIT3ryC9rPf
n27bva0FlHYYjGUMPRnALeOW9QKBgQC4jHVJeSxX6zyZKgoFtEdA+Y5GepW5pDxf
vMYPfgzxMv02WXT+XS3H+scgLlEMkQFUGAJImDb8a4c3f5mWMA19o2oeXhswU3GL
xss4oKpX350QWXY98c3x2Xoa6EnenSamMraU3k5PJl/JCwG8tAP8KxJCG3makAVY
AWsScpM5kQKBgC3zwhIrumm2IlKc57T/0TeUzO2eD9FM2xGMOJ5XoUIQzPmCwrKB
OEEfhSUI+HLi2xfCB3BTofXz2LBr2/wyJu38Ri96MDNMzP8HUyzE+FcCTh5keuKm
y0Yk8qL9h2s4wAahQoG20hW8REVCO7uK/RtLyvZYhNTdhWPd/l8CK+CpAoGAPRJ+
XeozuoSmcSlwV6LpUZ9Ebh5zHhQzxpr6EHZuYgO1uB6ykNrVI7Dh5MxvFfGGtwWa
+9sjlzUeckdP1FvimwlRPOt3o+55TQBkkEDbHmlp2NdCQg2bUcXemrj0eS8Yczel
JG7NC3RvjmCOGpyYFQNL74SYIYY3vn/5pwkzn4ECgYBjlp5GoqlJCtKDJKvYVHJV
vvqWTTd533D8vsQRNRP638aUwehIi7K6W90I+XZYNsoFl2geivMwaZJS0m0aJEXm
/8wQ2FnKa/dLUS1BFreWZ5vqMkvylh+Yr4UstZwp/SM3MCroTUQm0vj+prZNxd2g
RB6qyTEcPqEshBvz7Agptw==
-----END PRIVATE KEY-----";
        let private_key = RsaPrivateKey::from_pkcs8_pem(pem).unwrap();

        // match private_key {
        //     Ok(private_key) => {
        //         let signing_key = SigningKey::<sha2::Sha256>::new(private_key);
        //     },
        //     Err(private_key) => {
        //         info!("Key error {}", private_key);
        //     },
        // }

        let signing_key = SigningKey::<sha2::Sha256>::new(private_key);

        let bytes: &[u8] = b"rsa4096"; // HACK - the criterion is that the signature has leading zeros.
        let signature = signing_key.sign(bytes);

        info!("Signature: {}", signature);

        wasm::types::Action::Continue
    }
}
