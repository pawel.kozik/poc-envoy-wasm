# Envoy WASM plugin for Signatures

## Inspiration

- https://dev.to/satrobit/extending-envoy-with-webassembly-proxy-filters-1i96
- https://github.com/solo-io/proxy-runtime/tree/master

## FAQ

### Why fixing version of web assembly to lower version?

https://github.com/solo-io/proxy-runtime/issues/71

Error:

```
ERROR TS6054: File '~lib/@solo-io/proxy-runtime/proxy.ts' not found.
:
2 │ export * from "@solo-io/proxy-runtime/proxy";
│               ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
└─ in assembly/index.ts(2,15)
```
