// @ts-ignore
export * from "@solo-io/proxy-runtime/proxy";
import {
  RootContext,
  Context,
  registerRootContext,
  FilterHeadersStatusValues,
  stream_context,
} from "@solo-io/proxy-runtime";
import { sign } from "jsonwebtoken";

class AddHeaderRoot extends RootContext {
  createContext(context_id: u32): Context {
    return new AddHeader(context_id, this);
  }
}

class AddHeader extends Context {
  constructor(context_id: u32, root_context: AddHeaderRoot) {
    super(context_id, root_context);
  }
  onResponseHeaders(a: u32, end_of_stream: bool): FilterHeadersStatusValues {
    stream_context.headers.response.add("Hello", "World!");

    const privateKey = "";

    const payload = {
      transactionId: "aaa",
      amount: 100.0,
    };

    // const token = jsonwebtoken.sign(payload, privateKey, {
    //   algorithm: "PS256",
    //   header: {
    //     mat: Date.now(),
    //     mid: "unique-id",
    //     eid: "partner-x-id",
    //     crit: ["mat", "mid", "omid"],
    //   },
    //   noTimestamp: true, // omit iat in the payload
    // });

    return FilterHeadersStatusValues.Continue;
  }
}

registerRootContext((context_id: u32) => {
  return new AddHeaderRoot(context_id);
}, "add_header");
