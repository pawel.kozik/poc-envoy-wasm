module filter

go 1.16

require (
	github.com/go-jose/go-jose v2.6.2+incompatible // indirect
	github.com/golang-jwt/jwt/v5 v5.2.0 // indirect
	github.com/tetratelabs/proxy-wasm-go-sdk v0.22.0 // indirect
	golang.org/x/crypto v0.18.0 // indirect
	gopkg.in/go-jose/go-jose.v2 v2.6.2 // indirect
)
