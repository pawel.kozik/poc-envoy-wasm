package main

import (
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"log"

	"github.com/tetratelabs/proxy-wasm-go-sdk/proxywasm"
	"github.com/tetratelabs/proxy-wasm-go-sdk/proxywasm/types"
	//"github.com/golang-jwt/jwt/v5"
	"gopkg.in/go-jose/go-jose.v2"
)

const tickMilliseconds uint32 = 15000

var authHeader string

func main() {
	proxywasm.SetVMContext(&vmContext{})
}

type vmContext struct {
	// Embed the default VM context here,
	// so that we don't need to reimplement all the methods.
	types.DefaultVMContext
}

// Override types.DefaultVMContext.
func (*vmContext) NewPluginContext(contextID uint32) types.PluginContext {
	return &pluginContext{contextID: contextID}
}

type pluginContext struct {
	// Embed the default plugin context here,
	// so that we don't need to reimplement all the methods.
	types.DefaultPluginContext
	contextID uint32
	callBack  func(numHeaders, bodySize, numTrailers int)
}

// Override types.DefaultPluginContext.
func (*pluginContext) NewHttpContext(contextID uint32) types.HttpContext {
	return &httpAuthRandom{contextID: contextID}
}

type httpAuthRandom struct {
	// Embed the default http context here,
	// so that we don't need to reimplement all the methods.
	types.DefaultHttpContext
	contextID uint32
}

// Override types.DefaultPluginContext.
func (ctx *pluginContext) OnPluginStart(pluginConfigurationSize int) types.OnPluginStartStatus {
	if err := proxywasm.SetTickPeriodMilliSeconds(tickMilliseconds); err != nil {
		proxywasm.LogCriticalf("failed to set tick period: %v", err)
		return types.OnPluginStartStatusFailed
	}
	proxywasm.LogInfof("set tick period milliseconds: %d", tickMilliseconds)
	ctx.callBack = func(numHeaders, bodySize, numTrailers int) {
		respHeaders, _ := proxywasm.GetHttpCallResponseHeaders()
		proxywasm.LogInfof("respHeaders: %v", respHeaders)

		for _, headerPairs := range respHeaders {
			if headerPairs[0] == "authorization" {
				authHeader = headerPairs[1]
			}
		}
	}
	return types.OnPluginStartStatusOK
}

func (ctx *httpAuthRandom) OnHttpResponseHeaders(int, bool) types.Action {
	privKey, err := loadKey()

	if err != nil {
		proxywasm.LogCriticalf("sig failed: %v", err)
	}

	signer, err := jose.NewSigner(jose.SigningKey{Algorithm: jose.RS256, Key: privKey}, nil)
	if err != nil {
		panic(err)
	}

	var payload = []byte("Lorem ipsum dolor sit amet")
	object, err := signer.Sign(payload)
	if err != nil {
		panic(err)
	}

	serialized := object.FullSerialize()


	proxywasm.AddHttpResponseHeader("kevin-signature", serialized)



	// claims := jwt.MapClaims{
	// 	"a": "a",
	// }

	// token := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)
	// token.Header["typ"] = "JWS"

	// tokenString, err := token.SignedString(privKey)

	// // if err != nil {
	// // 	proxywasm.LogCriticalf("Error signing token: %v", err)
	// // }

	// proxywasm.LogCriticalf("Error signing token: %s", privKey.D.String())
	// proxywasm.LogCriticalf("Error signing token: %s", claims)


	// proxywasm.AddHttpResponseHeader("kevin-signature", tokenString)

	return types.ActionContinue
}

// Override types.DefaultPluginContext.
func (ctx *pluginContext) OnTick() {
	hs := [][2]string{
		{":method", "GET"}, {":authority", "some_authority"}, {":path", "/auth"}, {"accept", "*/*"},
	}
	if _, err := proxywasm.DispatchHttpCall("my_custom_svc", hs, nil, nil, 5000, ctx.callBack); err != nil {
		proxywasm.LogCriticalf("dispatch httpcall failed: %v", err)
	}
}

// func signatureMiddleware(privKey *rsa.PrivateKey) http.HandlerFunc {
// 	return func(w http.ResponseWriter, r *http.Request) {
// 		body, err := io.ReadAll(r.Body)

// 		if err != nil {
// 			log.Fatalf("Failed to read json from request body: %v", err)
// 		}
// 		defer r.Body.Close()

// 		var result map[string]interface{}

// 		jsonUnpacked := json.Unmarshal(body, &result)
// 		claims := jwt.MapClaims{jsonUnpacked}

// 		token := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)
// 		token.Header["typ"] = "JWS"

// 		tokenString, err := token.SignedString(privKey)

// 		if err != nil {
// 			log.Fatalf("Error signing token: %v", err)
// 		}

// 		w.Header().Add("kevin-signature", tokenString)
// 	}
// }

func loadKey() (*rsa.PrivateKey, error) {
	privateKeyPem := `-----BEGIN PRIVATE KEY-----
MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCoHRSZu/TBrWNn
ndR8uG1O4yQQbB4N1++eGVxmoj7/4r6EKnYpImH6Kd02vboW1NI3o0EuBMluE6fr
+/mY2DraapHRVTqiT28mkWD2JBcCQJ9JC4Jo5BfdZmH1zGv24huWNTdF46uOMCtB
Ie2l55CTJTWf6/VZt9nkPjAZ0Vl4EV4762iECQSO5GR62Lkcgfssah6nF2lccUlY
4bIsa0pgQo63GSuFTVBPxrQnu6T4N4ILXgntLv8FyA/a/qhdxWwrJraD7M5nT0UE
8pl3YO4dwZlmPVtKtEt2qEFoRDEJSZDIBqGOnkhxnS9pWr+zCRpWNvv6DvaQxRAv
FzRzNA3FAgMBAAECggEAC3EHO5Cjj3bp2Ah8M2IollY2gyOd8VqWmqT74+77k1YZ
mfcl2ZYseDvXLBLs56w5A8O0UO8kd73Auk/eOefQLENVL0Zkr4fxIafX+IiWREyk
9UdiQG9IVy97pUtHYc3GYN+4UPpgwlRG7Vo4yVzMOyeCJyMxAe0rAty0pgfYgKeu
2qInTtylfpa7WwdfVnr9XmKIjP+uiCkFmCAwAHQSLLhzmozZI65916ZooRd9eQbI
hms/X+e8wuWnVQvylc3o78m7kJkR6OI88XhqG0isTVGMmtl7zPi/RojHKS6/X7Ui
kx6LYhAcWC52y6aylJ6DtjLlGKeuDezfMjKpk7qAwQKBgQDpM6kNjiVL70UKQNQ6
Wc30JVuDaNCwSI+mERdS08kpjRWS9z3+QP8OJkO6V7p03YG8n11V+cf4gL8GJ/jd
WfEEJar9/ERNLDZQF9TyAezPfCgi8NcvpjLEQau1oC57JYfbjONQoOIT3ryC9rPf
n27bva0FlHYYjGUMPRnALeOW9QKBgQC4jHVJeSxX6zyZKgoFtEdA+Y5GepW5pDxf
vMYPfgzxMv02WXT+XS3H+scgLlEMkQFUGAJImDb8a4c3f5mWMA19o2oeXhswU3GL
xss4oKpX350QWXY98c3x2Xoa6EnenSamMraU3k5PJl/JCwG8tAP8KxJCG3makAVY
AWsScpM5kQKBgC3zwhIrumm2IlKc57T/0TeUzO2eD9FM2xGMOJ5XoUIQzPmCwrKB
OEEfhSUI+HLi2xfCB3BTofXz2LBr2/wyJu38Ri96MDNMzP8HUyzE+FcCTh5keuKm
y0Yk8qL9h2s4wAahQoG20hW8REVCO7uK/RtLyvZYhNTdhWPd/l8CK+CpAoGAPRJ+
XeozuoSmcSlwV6LpUZ9Ebh5zHhQzxpr6EHZuYgO1uB6ykNrVI7Dh5MxvFfGGtwWa
+9sjlzUeckdP1FvimwlRPOt3o+55TQBkkEDbHmlp2NdCQg2bUcXemrj0eS8Yczel
JG7NC3RvjmCOGpyYFQNL74SYIYY3vn/5pwkzn4ECgYBjlp5GoqlJCtKDJKvYVHJV
vvqWTTd533D8vsQRNRP638aUwehIi7K6W90I+XZYNsoFl2geivMwaZJS0m0aJEXm
/8wQ2FnKa/dLUS1BFreWZ5vqMkvylh+Yr4UstZwp/SM3MCroTUQm0vj+prZNxd2g
RB6qyTEcPqEshBvz7Agptw==
-----END PRIVATE KEY-----`

	block, _ := pem.Decode([]byte(privateKeyPem))

	if block == nil {
		log.Fatalf("Failed to parse PEM block containing the key")
	}

	privKey, err := x509.ParsePKCS8PrivateKey(block.Bytes)

	if err != nil {
		return nil, err
	}

	res := privKey.(*rsa.PrivateKey)

	return res, err
}
